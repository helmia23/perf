package perf;

public interface Command {
    void execute(String string);
}
