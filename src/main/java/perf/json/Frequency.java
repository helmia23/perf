package perf.json;

import java.util.concurrent.TimeUnit;

public class Frequency {
    public double duration;
    public int orders;
    public double stdDeviationPercent;
    public long period;
    public TimeUnit timeUnit;
    public String mode;
}
