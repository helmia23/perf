package perf.json;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Config {
    public Frequency frequency;
    public Mq mq;
    public Amq amq;

    public static Config getConfig() {
        Gson gson = new Gson();
        Config config = new Config();
        File file = new File("config.json");

        System.out.println(file.getAbsoluteFile());

        try (FileReader json = new FileReader(file);) {
            config = gson.fromJson(json, Config.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return config;
    }
}
