package perf.json;

public class Mq implements AutoCloseable{
    public String hostname;
    public String channel;
    public String port;
    public String queueManager;
    public int transportType;
    public String queueName;
    public String username;
    public String password;

    @Override
    public void close() throws Exception {
        System.out.println("no mq config");
    }
}
