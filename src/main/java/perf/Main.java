package perf;

import perf.freq.Frequencer;
import perf.json.Config;
import perf.mq.Consumer;
import perf.mq.Producer;

public class Main {

    public static void main(String[] args) {
        Config config = Config.getConfig();
        Consumer.startListening();
        final Producer producer = new Producer(config);
        Command command = new Command() {
            @Override
            public void execute(String message) {
                producer.send(message);
            }
        };
        Frequencer.run(command, config, new String[]{"a", "b", "c"});
    }
}
