package perf.mq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;

public class Consumer implements MessageListener {


    private Consumer() {
        DefaultMessageListenerContainer listenerContainer = new DefaultMessageListenerContainer();
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://127.0.0.1:61116?wireFormat.tcpNoDelayEnabled=true");
        listenerContainer.setConnectionFactory(connectionFactory);
        try {
            listenerContainer.setDestination(connectionFactory
                    .createConnection()
                    .createSession(true, Session.AUTO_ACKNOWLEDGE)
                    .createQueue("test.foo"));
        } catch (JMSException e) {
            e.printStackTrace();
        }
        listenerContainer.setConcurrentConsumers(5);
        listenerContainer.setIdleConsumerLimit(5);
        listenerContainer.setMessageListener(this);
        listenerContainer.initialize();
        listenerContainer.start();
    }

    public static void startListening() {
        new Consumer();
    }


    @Override
    public void onMessage(final Message message) {
        System.out.println(message);
    }
}