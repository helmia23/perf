package perf.mq;

import com.ibm.mq.jms.MQQueueConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.springframework.jms.core.JmsTemplate;
import perf.json.Amq;
import perf.json.Config;
import perf.json.Mq;

import javax.jms.*;

public class Producer {

    private JmsTemplate jmsTemplate;

    public Producer(Config config) {
       /* try  {
            Mq mq = config.mq;
            MQQueueConnectionFactory mqQueueConnectionFactory = new MQQueueConnectionFactory();
            mqQueueConnectionFactory.setHostName(mq.hostname);
            mqQueueConnectionFactory.setChannel(mq.channel);
            mqQueueConnectionFactory.setPort(Integer.parseInt(mq.port));
            mqQueueConnectionFactory.setQueueManager(mq.queueManager);
            mqQueueConnectionFactory.setTransportType(mq.transportType);

            UserCredentialsConnectionFactoryAdapter connectionFactoryAdapter = new UserCredentialsConnectionFactoryAdapter();
            connectionFactoryAdapter.setTargetConnectionFactory(mqQueueConnectionFactory);
            connectionFactoryAdapter.setUsername(mq.username);
            connectionFactoryAdapter.setPassword(mq.password);

            jmsTemplate = new JmsTemplate(connectionFactoryAdapter);
            jmsTemplate.setDefaultDestination(connectionFactoryAdapter
                    .createConnection()
                    .createSession(true, Session.AUTO_ACKNOWLEDGE)
                    .createQueue(mq.queueName));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        try{
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://127.0.0.1:61116?wireFormat.tcpNoDelayEnabled=true");//&wireFormat.maxInactivityDuration=50000&;wireFormat.maxInactivityDurationInitalDelay=30000&;jms.alwaysSyncSend=true&;jms.optimizeAcknowledge=true");
            UserCredentialsConnectionFactoryAdapter connectionFactoryAdapter = new UserCredentialsConnectionFactoryAdapter();
            connectionFactoryAdapter.setTargetConnectionFactory(connectionFactory);
            jmsTemplate = new JmsTemplate(connectionFactoryAdapter);
            jmsTemplate.setDefaultDestination(connectionFactoryAdapter
                    .createConnection()
                    .createSession(true, Session.AUTO_ACKNOWLEDGE)
                    .createQueue("test.foo"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void send(final String message) {
//        System.out.println("message "+message);
        jmsTemplate.convertAndSend(message);
    }
}
