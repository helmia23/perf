package perf.freq;


import com.google.common.collect.Iterators;
import perf.Command;
import perf.json.Config;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

class FrequencerRunner implements Runnable {

    private List<Double> yValues;
    private AtomicInteger xValues;
    private Iterator<String> scenario;
    private ScheduledExecutorService scheduledExecutorService;
    private Command command;

    FrequencerRunner( Command command,Config config, String[] scenarios, ScheduledExecutorService scheduledExecutorService) {
        this.yValues = GraphMode.valueOf(config.frequency.mode).getYValues(config);
        this.xValues = new AtomicInteger();
        this.scenario = Iterators.cycle(scenarios);
        this.scheduledExecutorService = scheduledExecutorService;
        this.command = command;
    }

    @Override
    public void run() {
        if (xValues.getAndIncrement() < yValues.size()) {
            double mopsFrequency = yValues.get(xValues.get());
            if (frequencyIsMoreThanZero(mopsFrequency)) {
                double period = 1 / mopsFrequency * 1000;//todo this is wrong for other than mili. need real converter object
                scheduledExecutorService
                        .scheduleAtFixedRate(new InnerFrequencerRunner(Math.round(mopsFrequency),
                                        Executors.newSingleThreadScheduledExecutor()),
                                0,
                                (long) period,
                                TimeUnit.MILLISECONDS);
            }
        } else {
            scheduledExecutorService.shutdown();
        }
    }

    private boolean frequencyIsMoreThanZero(double mopsFrequency) {
        return Math.round(mopsFrequency) > 0;
    }

    private class InnerFrequencerRunner implements Runnable {
        private final AtomicLong mopCount;
        private final ScheduledExecutorService innerScheduledExecutorService;

        InnerFrequencerRunner(long mopCount, ScheduledExecutorService innerScheduledExecutorService) {
            this.mopCount = new AtomicLong(mopCount);
            this.innerScheduledExecutorService = innerScheduledExecutorService;
        }

        @Override
        public void run() {
            if (mopCount.decrementAndGet() > 0) {
                command.execute(scenario.next());
            } else {
                innerScheduledExecutorService.shutdown();
            }
        }
    }
}
