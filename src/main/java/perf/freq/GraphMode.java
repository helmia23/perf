package perf.freq;

import perf.json.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

enum GraphMode {
    Gaussian {
        @Override
        List<Double> getYValues(Config config) {
            double stdDeviation = config.frequency.stdDeviationPercent * config.frequency.duration;
            Collection<Double> xLine = new ArrayList<>();
            Double sum = 0.0;

            for (Double x = 1.0; x < config.frequency.duration; x++) {
                xLine.add(x);
                sum += x;
            }

            Gaussian gaussian = new Gaussian(stdDeviation, sum / config.frequency.duration);
            List<Double> list = new ArrayList<>();

            for (Double doubleX : xLine) {

                double doubleY = gaussian.getY(doubleX);
                double normalY = doubleY * config.frequency.orders;

                list.add(normalY);
            }
            return list;
        }
    },
    Cubic {
        @Override
        List<Double> getYValues(Config config) {
            List<Double> yValues = new ArrayList<>();
            for (int x = 0; x < config.frequency.duration; x++) {
                yValues.add(Math.pow(x, 2));
            }
            return yValues;
        }
    },
    Linear {
        @Override
        List<Double> getYValues(Config config) {
            List<Double> yValues = new ArrayList<>();
            for (int x = 0; x < config.frequency.duration; x++) {
                yValues.add((double) x);
            }
            return yValues;
        }
    },
    Exponent {
        @Override
        List<Double> getYValues(Config config) {
            List<Double> yValues = new ArrayList<>();
            for (Double x = config.frequency.duration; x > 0; x--) {
                yValues.add(Math.exp(x));
            }
            return yValues;
        }
    };

    abstract List<Double> getYValues(Config config);
}
