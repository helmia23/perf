package perf.freq;


import perf.Command;
import perf.graph.Graph;
import perf.json.Config;
import perf.json.Frequency;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Frequencer {

    public static void run(Command command, Config config, String[] scenarios) {

//        Graph.drawGraph(yValues);

        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        scheduledExecutorService.scheduleAtFixedRate(new FrequencerRunner(command,
                        config,
                        scenarios,
                        scheduledExecutorService),
                0,
                config.frequency.period,
                config.frequency.timeUnit);
    }
}
