package perf.freq;

public class Gaussian {

    private Double stdDeviation, variance, mean;

    Gaussian(Double stdDeviation, Double mean) {
        this.stdDeviation = stdDeviation;
        variance = stdDeviation * stdDeviation;
        this.mean = mean;
    }

    double getY(Double x) {

        return Math.pow(Math.exp(-(((x - mean) * (x - mean)) / ((2 * variance)))), 1 / (stdDeviation * Math.sqrt(2 * Math.PI)));

    }
}